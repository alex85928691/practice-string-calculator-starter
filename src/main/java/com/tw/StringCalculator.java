package com.tw;

public class StringCalculator {
    public int add(String string) {
        String[] results = string.split("\\D+");
        int number =0;
        for(String result : results){
            if(result != ""){
                number += Integer.parseInt(result);
            }
        }

        return number;
    }
}